package t1

import (
	"fmt"

	"bitbucket.org/bigwhite/w"
)

func T() {
	fmt.Println("T1 v1.0.0")
	w.W()
}
